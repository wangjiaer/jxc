package com.atguigu.jxc.util.result;

import lombok.Data;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class Result implements Serializable {
    private static final long serialVersionUID = 1L;
    private static String code;
    private static String msg;


    public static Map<String,Object> put(List list) {
        String total;
        Map<String, Object> resultMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(list)) {
            total= String.valueOf(list.size());
        } else {
            total = "0";
        }
        resultMap.put("total",total);
        resultMap.put("rows",list);
        return resultMap;
    }

    public static Map<String,Object> ok() {
        Map<String, Object> resultMap = new HashMap<>();
        code = ResultCodeEnum.SUCCESS.getCode();
        msg = ResultCodeEnum.SUCCESS.getMessage();
        resultMap.put("code",code);
        resultMap.put("msg",msg);
        return resultMap;
    }
    public static Map<String,Object> fail() {
        Map<String, Object> resultMap = new HashMap<>();
        code = ResultCodeEnum.FAIL.getCode();
        msg = ResultCodeEnum.FAIL.getMessage();
        resultMap.put("code",code);
        resultMap.put("msg",msg);
        return resultMap;
    }
}
