package com.atguigu.jxc.util.result;

import lombok.Getter;

@Getter
public enum ResultCodeEnum {
    SUCCESS("100","请求成功"),
    FAIL("101", "请求失败");
    private String code;
    private String message;
    ResultCodeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
