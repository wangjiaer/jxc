package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface SupplierDao {

    List<Supplier> getSupplierList(Integer page, Integer rows, String supplierName);


    void saveSupplier(Supplier supplier);

    void modifySupplier(@Param("supplierId") String supplierId, @Param("supplierName") String supplierName, @Param("contacts") String contacts, @Param("address") String address, @Param("remarks") String remarks, @Param("phoneNumber") String phoneNumber);

    void deleteSupplier(@Param("ids") String ids);
}
