package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品类别
 */
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);


    void saveGoodsType(@Param("goodsTypeName") String goodsTypeName, @Param("pId") Integer pId, @Param("goodsTypeState") int goodsTypeState);

    void deleteGoodsType(Integer goodsTypeId);

    Integer getPIdByGoodsTypeId(@Param("goodsTypeId") Integer goodsTypeId);
}
