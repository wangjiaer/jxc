package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();


    List<Goods> getInventoryList(@Param("page") Integer page, @Param("rows") Integer rows, @Param("codeOrName") String codeOrName, @Param("goodsTypeId") Integer goodsTypeId, @Param("matches") boolean matches);

    List<Goods> getGoodsList(@Param("page") Integer page, @Param("rows") Integer rows, @Param("goodsName") String goodsName, @Param("goodsTypeId") Integer goodsTypeId);

    void insertGoods(Goods goods);


    void updateGoods(@Param("goodsId") String goodsId,@Param("goodsCode") String goodsCode,@Param("goodsName") String goodsName,@Param("minNum") Integer minNum,@Param("goodsModel") String goodsModel,@Param("goodsProducer") String goodsProducer,@Param("purchasingPrice") double purchasingPrice,@Param("remarks") String remarks,@Param("sellingPrice") double sellingPrice,@Param("goodsUnit") String goodsUnit,@Param("goodsTypeId") Integer goodsTypeId);

    void deleteGoods(Integer goodsId);

    Goods selectGoods(Integer goodsId);

    void saveStock(@Param("goodsId") Integer goodsId,@Param("inventoryQuantity") Integer inventoryQuantity,@Param("purchasingPrice") double purchasingPrice);

    void deleteStock(Integer goodsId);

    List<Goods> getAllGoods();
}
