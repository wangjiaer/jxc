package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OverflowListGoodsDao {
    void insertOverflowListGoodsList(@Param("overflowListGoodsList") List<OverflowListGoods> overflowListGoodsList);

    List<OverflowListGoods> getOverflowListGoodsList(@Param("overflowListId") Integer overflowListId);
}
