package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CustomerDao {
    List<Customer> getCustomerList(Integer page, Integer rows, String customerName);

    void insertCustomer(Customer customer);

    void updateCustomer(@Param("customerId") String customerId,@Param("customerName") String customerName,@Param("contacts") String contacts,@Param("phoneNumber") String phoneNumber,@Param("address") String address,@Param("remarks") String remarks);

    void deleteCustomer(@Param("ids") String ids);
}
