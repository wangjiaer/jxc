package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DamageListDao {
    Integer insertDamageList(DamageList damageList);

    List<DamageList> getDamageList(@Param("sTime") String sTime, @Param("eTime") String eTime);
}
