package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageListGoods;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DamageListGoodsDao {
    List<DamageListGoods> getDamageListGoodsList(@Param("damageListId") Integer damageListId);

    void insertDamageListGoodsList(@Param("damageListGoodsList") List<DamageListGoods> damageListGoodsList);
}
