package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.OverflowList;

import java.util.List;

public interface OverflowListService {
    Integer insertOverflowList(OverflowList overflowList);

    List<OverflowList> getOverflowListGoods(String sTime, String eTime);
}
