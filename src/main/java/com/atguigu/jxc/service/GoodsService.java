package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;

import java.util.List;
import java.util.Map;

public interface GoodsService {


    ServiceVO getCode();


    List<Goods> getInventoryList(Integer page, Integer rows, String codeOrName, Integer goodsTypeId,boolean matches);

    List<Goods> getGoodsList(Integer page, Integer rows, String goodsName, Integer goodsTypeId);

    void insertOrUpdateGoods(String goodsId,Goods goods);

    String deleteGoods(Integer goodsId);

    void saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);

    void deleteStock(Integer goodsId);

    List<Goods> getAllGoods();
}
