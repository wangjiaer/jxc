package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.OverflowListGoods;

import java.util.List;

public interface OverflowListGoodsService {
    void insertOverflowListGoodsList(List<OverflowListGoods> overflowListGoodsList);

    List<OverflowListGoods> getOverflowListGoodsList(Integer overflowListId);
}
