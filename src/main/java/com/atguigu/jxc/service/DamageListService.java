package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageList;

import java.util.List;

public interface DamageListService {
    Integer insertDamageList(DamageList damageList);

    List<DamageList> getDamageList(String sTime, String eTime);
}
