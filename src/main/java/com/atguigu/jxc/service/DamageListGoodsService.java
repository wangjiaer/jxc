package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageListGoods;

import java.util.List;

public interface DamageListGoodsService {
    void insertDamageListGoodsList(List<DamageListGoods> damageListGoodsList);

    List<DamageListGoods> getDamageListGoodsList(Integer damageListId);
}
