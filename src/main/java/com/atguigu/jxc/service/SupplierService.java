package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Supplier;

import java.util.List;

public interface SupplierService {


    void saveOrModifySupplier(String supplierId, Supplier supplier);

    List<Supplier> getSupplierList(Integer page, Integer rows, String supplierName);

    void deleteSupplier(String ids);
}
