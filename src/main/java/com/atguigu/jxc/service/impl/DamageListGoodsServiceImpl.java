package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.service.DamageListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DamageListGoodsServiceImpl implements DamageListGoodsService {
    @Autowired
    private DamageListGoodsDao damageListGoodsDao;
    @Override
    public void insertDamageListGoodsList(List<DamageListGoods> damageListGoodsList) {
        damageListGoodsDao.insertDamageListGoodsList(damageListGoodsList);
    }

    @Override
    public List<DamageListGoods> getDamageListGoodsList(Integer damageListId) {
        return damageListGoodsDao.getDamageListGoodsList(damageListId);
    }
}
