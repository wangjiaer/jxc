package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.OverflowListDao;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.service.OverflowListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OverflowListServiceImpl implements OverflowListService {
    @Autowired
    private OverflowListDao overflowListDao;
    @Override
    public Integer insertOverflowList(OverflowList overflowList) {
        overflowListDao.insertOverflowList(overflowList);
        Integer overflowListId = overflowList.getOverflowListId();
        return overflowListId;
    }

    @Override
    public List<OverflowList> getOverflowListGoods(String sTime, String eTime) {
        return overflowListDao.getOverflowListGoods(sTime,eTime);
    }
}
