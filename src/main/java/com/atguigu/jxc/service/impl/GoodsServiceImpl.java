package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import com.atguigu.jxc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for (int i = 4; i > intCode.toString().length(); i--) {

            unitCode = "0" + unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    public List<Goods> getInventoryList(Integer page, Integer rows, String codeOrName, Integer goodsTypeId, boolean matches) {
        page = (page - 1) * rows;
        List<Goods> inventoryList = goodsDao.getInventoryList(page, rows, codeOrName, goodsTypeId, matches);
        return inventoryList;
    }

    @Override
    public List<Goods> getGoodsList(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        page = (page - 1) * rows;
        List<Goods> goodsList = goodsDao.getGoodsList(page, rows, goodsName, goodsTypeId);
        return goodsList;
    }

    @Override
    public void insertOrUpdateGoods(String goodsId, Goods goods) {
        if (goodsId == null || StringUtil.isEmpty(goodsId)) {
            //新增操作
            goodsDao.insertGoods(goods);
        } else {
            //修改操作
            String goodsCode = goods.getGoodsCode();
            String goodsName = goods.getGoodsName();
            Integer minNum = goods.getMinNum();
            String goodsModel = goods.getGoodsModel();
            String goodsProducer = goods.getGoodsProducer();
            double purchasingPrice = goods.getPurchasingPrice();
            String remarks = goods.getRemarks();
            double sellingPrice = goods.getSellingPrice();
            String goodsUnit = goods.getGoodsUnit();
            Integer goodsTypeId = goods.getGoodsTypeId();
            goodsDao.updateGoods(goodsId, goodsCode, goodsName, minNum, goodsModel, goodsProducer, purchasingPrice, remarks, sellingPrice, goodsUnit, goodsTypeId);
        }

    }

    @Override
    public String deleteGoods(Integer goodsId) {
        Goods goods = goodsDao.selectGoods(goodsId);
        if (goods.getState() == 0) {
            goodsDao.deleteGoods(goodsId);
            return "请求成功";
        }
        return "删除失败：商品入库或有进货或有销售单据";
    }

    @Override
    public void saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        goodsDao.saveStock(goodsId, inventoryQuantity, purchasingPrice);
    }

    @Override
    public void deleteStock(Integer goodsId) {
        Goods goods = goodsDao.selectGoods(goodsId);
        if (goods.getState() == 0) {
            goodsDao.deleteStock(goodsId);
        }
    }

    @Override
    public List<Goods> getAllGoods() {
        return goodsDao.getAllGoods();
    }

}
