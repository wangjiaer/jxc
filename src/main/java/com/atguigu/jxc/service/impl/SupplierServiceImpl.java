package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import com.atguigu.jxc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

@Service
public class SupplierServiceImpl implements SupplierService {
    @Autowired
    private SupplierDao supplierDao;

    @Override
    public void saveOrModifySupplier(String supplierId, Supplier supplier) {
        if (supplierId==null|| StringUtil.isEmpty(supplierId)) {
            //新增操作
            supplierDao.saveSupplier(supplier);
        } else {
            //修改操作
            String supplierName = supplier.getSupplierName();
            String contacts = supplier.getContacts();
            String address = supplier.getAddress();
            String remarks = supplier.getRemarks();
            String phoneNumber = supplier.getPhoneNumber();
            supplierDao.modifySupplier(supplierId,supplierName,contacts,address,remarks,phoneNumber);
        }

    }

    @Override
    public List<Supplier> getSupplierList(Integer page, Integer rows, String supplierName) {
        page = (page - 1) * rows;
        List<Supplier> supplierList = supplierDao.getSupplierList(page,rows,supplierName);
        return supplierList;
    }

    @Override
    public void deleteSupplier(String ids) {
        supplierDao.deleteSupplier(ids);
    }


}
