package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageListDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.service.DamageListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DamageListServiceImpl implements DamageListService {
    @Autowired
    private DamageListDao damageListDao;
    @Override
    public Integer insertDamageList(DamageList damageList) {
        damageListDao.insertDamageList(damageList);
        Integer damageListId = damageList.getDamageListId();
        return damageListId;
    }

    @Override
    public List<DamageList> getDamageList(String sTime, String eTime) {
        List<DamageList> damageList = damageListDao.getDamageList(sTime,eTime);
        return damageList;
    }
}
