package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import com.atguigu.jxc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerDao customerDao;

    @Override
    public List<Customer> getCustomerList(Integer page, Integer rows, String customerName) {
        page = (page - 1) * rows;
        List<Customer> customerList = customerDao.getCustomerList(page, rows, customerName);
        return customerList;
    }

    @Override
    public void saveOrUpdateCustomer(String customerId, Customer customer) {
        if (customerId == null || StringUtil.isEmpty(customerId)) {
            //新增操作
            customerDao.insertCustomer(customer);
        } else {
            String customerName = customer.getCustomerName();
            String contacts = customer.getContacts();
            String phoneNumber = customer.getPhoneNumber();
            String address = customer.getAddress();
            String remarks = customer.getRemarks();
            customerDao.updateCustomer(customerId,customerName,contacts,phoneNumber,address,remarks);
        }
    }

    @Override
    public void deleteCustomer(String ids) {
        customerDao.deleteCustomer(ids);
    }
}
