package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;

import java.util.ArrayList;

/**
 * @description
 */
public interface GoodsTypeService {
    ArrayList<Object> loadGoodsType();

    void saveGoodsType(String goodsTypeName, Integer pId);

    void deleteGoodsType(Integer goodsTypeId);
}
