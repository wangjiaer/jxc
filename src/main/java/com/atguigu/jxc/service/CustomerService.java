package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Customer;

import java.util.List;

public interface CustomerService {
    List<Customer> getCustomerList(Integer page, Integer rows, String customerName);

    void saveOrUpdateCustomer(String customerId, Customer customer);

    void deleteCustomer(String ids);
}
