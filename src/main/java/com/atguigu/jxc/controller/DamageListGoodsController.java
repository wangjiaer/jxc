package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.service.DamageListGoodsService;
import com.atguigu.jxc.service.DamageListService;
import com.atguigu.jxc.util.result.Result;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/damageListGoods")
public class DamageListGoodsController {
    @Autowired
    private DamageListGoodsService damageListGoodsService;
    @Autowired
    private DamageListService damageListService;

    @PostMapping("/save")
    public ServiceVO saveDamageListGoods(@RequestParam("damageNumber") String damageNumber,
                                         DamageList damageList,
                                         String damageListGoodsStr) {

        damageList.setDamageNumber(damageNumber);
        Integer damageListId = damageListService.insertDamageList(damageList);
        Gson gson = new Gson();
        Type type = new TypeToken<List<DamageListGoods>>() {
        }.getType();
        List<DamageListGoods> DamageListGoodsList = gson.fromJson(damageListGoodsStr, type);
        for (DamageListGoods damageListGoods : DamageListGoodsList) {
            damageListGoods.setDamageListId(damageListId);
        }
        damageListGoodsService.insertDamageListGoodsList(DamageListGoodsList);
        return new ServiceVO(100, "请求成功");
    }

    //damageListGoods/list
    @PostMapping("/list")
    public Map<String, Object> getDamageList(String sTime, String eTime) {
        List<DamageList> damageList = damageListService.getDamageList(sTime,eTime);
        return Result.put(damageList);
    }
    //damageListGoods/goodsList
    @PostMapping("/goodsList")
    public Map<String, Object> getDamageListGoodsList(Integer damageListId) {
        List<DamageListGoods> damageListGoodsList = damageListGoodsService.getDamageListGoodsList(damageListId);
        return Result.put(damageListGoodsList);
    }
}
