package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.util.result.Result;
import com.atguigu.jxc.util.StringUtil;
import io.swagger.models.auth.In;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @description 商品信息Controller
 */
@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    /**
     * 分页查询商品库存信息
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param codeOrName  商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping("/listInventory")
    public Map<String, Object> getInventoryList(Integer page,
                                                Integer rows,
                                                String codeOrName,
                                                Integer goodsTypeId) {
        String regStr = "^\\d{4}$";
        boolean matches;

        if (!StringUtil.isEmpty(codeOrName)) {
            matches = codeOrName.matches(regStr);
        } else {
            matches = false;
        }


        List<Goods> inventoryList = goodsService.getInventoryList(page, rows, codeOrName, goodsTypeId, matches);
        Map<String, Object> resultMap = Result.put(inventoryList);

        return resultMap;
    }

    /**
     * 分页查询商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param goodsName 商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping("/list")
    public Map<String,Object> getGoodsList(Integer page,
                                           Integer rows,
                                           String goodsName,
                                           Integer goodsTypeId) {
        List<Goods> goodsList = goodsService.getGoodsList(page,rows,goodsName,goodsTypeId);
        return Result.put(goodsList);
    }

    //商品添加或修改
    //goods/save?goodsId=37
    @PostMapping("/save")
    public ServiceVO insertOrUpdateGoods(@RequestParam(value = "goodsId",required = false)String goodsId,
                                         Goods goods) {
        goodsService.insertOrUpdateGoods(goodsId,goods);
        return new ServiceVO(100,"请求成功");
    }
    /**
     * 生成商品编码
     *
     * @return
     */
    @RequestMapping("/getCode")
    @RequiresPermissions(value = "商品管理")
    public ServiceVO getCode() {
        return goodsService.getCode();
    }

    /**
     * 添加或修改商品信息
     * @param goods 商品信息实体
     * @return
     */

    /**
     * 删除商品信息
     * @param goodsId 商品ID
     * @return
     */
    @RequestMapping("/delete")
    public ServiceVO deleteGoods(Integer goodsId) {
        String msg = goodsService.deleteGoods(goodsId);
        return new ServiceVO(100,msg);
    }

    /**
     * 分页查询无库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    //goods/getNoInventoryQuantity
    @PostMapping("/getNoInventoryQuantity")
    public Map<String,Object> getNoInventoryQuantity(Integer page,Integer rows,String nameOrCode) {
        String regStr = "^\\d{4}$";
        Integer goodsTypeId = null;
        boolean matches;

        if (!StringUtil.isEmpty(nameOrCode)) {
            matches = nameOrCode.matches(regStr);
        } else {
            matches = false;
        }
        List<Goods> inventoryList = goodsService.getInventoryList(page, rows, nameOrCode, goodsTypeId, matches);
        List<Goods> noInventoryList = inventoryList.stream().filter(goods ->
            goods.getInventoryQuantity()<=0
        ).collect(Collectors.toList());
        Map<String, Object> resultMap = Result.put(noInventoryList);
        return resultMap;
    }


    /**
     * 分页查询有库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    //goods/getHasInventoryQuantity
    @PostMapping("/getHasInventoryQuantity")
    public Map<String,Object> getHasInventoryQuantity(Integer page,Integer rows,String nameOrCode) {
        String regStr = "^\\d{4}$";
        Integer goodsTypeId = null;
        boolean matches;

        if (!StringUtil.isEmpty(nameOrCode)) {
            matches = nameOrCode.matches(regStr);
        } else {
            matches = false;
        }
        List<Goods> inventoryList = goodsService.getInventoryList(page, rows, nameOrCode, goodsTypeId, matches);
        List<Goods> hasInventoryList = inventoryList.stream().filter(goods ->
                goods.getInventoryQuantity()>0
        ).collect(Collectors.toList());
        Map<String, Object> resultMap = Result.put(hasInventoryList);
        return resultMap;
    }


    /**
     * 添加库存、修改数量或成本价
     * @param goodsId 商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice 成本价
     * @return
     */
    //goods/saveStock?goodsId=25
    @PostMapping("/saveStock")
    public ServiceVO saveStock(@RequestParam("goodsId") Integer goodsId,
                               Integer inventoryQuantity,
                               double purchasingPrice) {
        goodsService.saveStock(goodsId,inventoryQuantity,purchasingPrice);
        return new ServiceVO(100,"请求成功");
    }

    /**
     * 删除商品库存
     * @param goodsId 商品ID
     * @return
     */
    //goods/deleteStock
    @PostMapping("/deleteStock")
    public ServiceVO deleteStock(Integer goodsId) {
        goodsService.deleteStock(goodsId);
        return new ServiceVO(100,"请求成功");
    }

    /**
     * 查询库存报警商品信息
     * @return
     */
    //goods/listAlarm
    @PostMapping("/listAlarm")
    public Map<String,Object> listAlarm() {
        List<Goods> allGoodsList = goodsService.getAllGoods();
        List<Goods> goodsList = allGoodsList.stream().filter(goods -> goods.getInventoryQuantity() < goods.getMinNum()).collect(Collectors.toList());
        return Result.put(goodsList);
    }

}
