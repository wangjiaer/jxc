package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import com.atguigu.jxc.util.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/customer")
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    //客户列表分页（名称模糊查询）
    @PostMapping("/list")
    public Map<String, Object> getCustomerList(Integer page,
                                               Integer rows,
                                               String customerName) {
        List<Customer> customerList = customerService.getCustomerList(page, rows, customerName);
        return Result.put(customerList);
    }

    //客户添加或修改
    //customer/save?customerId=1
    @PostMapping("/save")
    public ServiceVO saveOrUpdateCustomer(@RequestParam(value = "customerId", required = false) String customerId,
                                          Customer customer) {
        customerService.saveOrUpdateCustomer(customerId,customer);
        return new ServiceVO(100,"请求成功");
    }
    //客户删除（支持批量删除）
    //customer/delete
    @PostMapping("/delete")
    public ServiceVO deleteCustomer (String ids) {
        customerService.deleteCustomer(ids);
        return new ServiceVO(100,"请求成功");
    }

}
