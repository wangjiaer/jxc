package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.service.OverflowListGoodsService;
import com.atguigu.jxc.service.OverflowListService;
import com.atguigu.jxc.util.result.Result;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/overflowListGoods")
public class OverflowListGoodsController {
    @Autowired
    private OverflowListService overflowListService;
    @Autowired
    private OverflowListGoodsService overflowListGoodsService;
    //overflowListGoods/save?overflowNumber=BY1605767033015
    @PostMapping("/save")
    public ServiceVO saveOverflowListGoods(@RequestParam("overflowNumber") String overflowNumber,
                                           OverflowList overflowList,
                                           String overflowListGoodsStr) {
        overflowList.setOverflowNumber(overflowNumber);
        Integer overflowListId = overflowListService.insertOverflowList(overflowList);
        Gson gson = new Gson();
        Type type = new TypeToken<List<OverflowListGoods>>() {}.getType();
        List<OverflowListGoods> overflowListGoodsList = gson.fromJson(overflowListGoodsStr, type);
        for (OverflowListGoods overflowListGoods : overflowListGoodsList) {
            overflowListGoods.setOverflowListId(overflowListId);
        }
        overflowListGoodsService.insertOverflowListGoodsList(overflowListGoodsList);
        return new ServiceVO(100,"请求成功");
    }
    //overflowListGoods/list
    @PostMapping("/list")
    public Map<String,Object> getOverflowListGoods (String  sTime, String  eTime){
        List<OverflowList> overflowList = overflowListService.getOverflowListGoods(sTime,eTime);
        return Result.put(overflowList);
    }
    //overflowListGoods/goodsList
    @PostMapping("/goodsList")
    public Map<String,Object> getOverflowListGoodsList (Integer overflowListId) {
        List<OverflowListGoods> overflowListGoodsList = overflowListGoodsService.getOverflowListGoodsList(overflowListId);
        return Result.put(overflowListGoodsList);
    }
}
