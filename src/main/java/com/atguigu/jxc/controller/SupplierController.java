package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import com.atguigu.jxc.util.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/supplier")
public class SupplierController {
    @Autowired
    private SupplierService supplierService;
    //分页查询供应商
    @PostMapping("/list")
    public Map<String,Object> getSupplierList(Integer page,Integer rows,String supplierName) {
        List<Supplier> supplierList = supplierService.getSupplierList(page,rows,supplierName);
        Map<String, Object> resultMap = Result.put(supplierList);
        return resultMap;
    }

    //供应商添加或修改
    @PostMapping("/save")
    public ServiceVO saveOrModifySupplier(@RequestParam(value = "supplierId",required = false) String supplierId,
                                          Supplier supplier) {
        supplierService.saveOrModifySupplier(supplierId,supplier);

        return new ServiceVO(100,"请求成功");
    }
    //删除供应商（支持批量删除）
    @PostMapping("/delete")
    public ServiceVO deleteSupplier(String ids) {
        supplierService.deleteSupplier(ids);
        return new ServiceVO(100,"请求成功");
    }



}
